// require our dependencies
var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

module.exports = {
	// the base directory (absolute path) for resolving the entry options
	context: __dirname,
	// the entry point we created earlier. Note that './' means
	// your current directory. You don't have to specify the extension now,
	// because you will specifiy extensions later in the 'resolve' section
	entry: [
	  'webpack-dev-server/client?http://localhost:3000',
	  'webpack/hot/only-dev-server',
		'./src/index',
	],

	output: {
		//where you want your compiled bundle to be stored
		// path: path.resolve('../static/js/bundles'),
		path: path.resolve('../../../static/client/js/bundles'),
		// naming convention webpack should use for your files
		filename: '[name]-[hash].bundle.js',
		publicPath: 'http://localhost:3000/assets/bundles/', // Tell django to use this URL to load packages and not use STATIC_URL + bundle_name
	},

	plugins: [
    new webpack.HotModuleReplacementPlugin(),
		//tells webpack where to store data about your bundles.
		new webpack.NoErrorsPlugin(), // don't reload if there is an error
		new BundleTracker({filename: './webpack-stats-client.json'}),
		//make jQuery available in every module
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		})
	],

	module: {
		loaders: [
			//a regexp that tells webpack use the following loaders
			//.js and .jsx files
			{test: /\.jsx?$/,
				// we definitely don't want babel to transpile all the files in
				// node_modules. That would take a long time.
				exclude: /node_modules/,
				// use the babel loader
				loaders: ['react-hot','babel'],
			},
			{
		        test: /.jsx?$/,
		        loader: 'babel-loader',
				exclude: /node_modules/,
		        query: {
		          presets: ['es2015', 'react']
		        }
		     }
		]
	},

	resolve: {
		//tells webpack where to look for modules
		modulesDirectories: ['node_modules'],
		//extensions that should be used to resolve modules
		extensions: ['', '.js', '.jsx']
	}
}