var keyMirror = require('keymirror');

module.exports = {

	ActionTypes: keyMirror({
	  LIST_USERS: null,
	  LIST_USER: null,
	  ADD_MESSAGE: null,
	  CREATE_CONVERSATION: null,
	  LOGIN: null,
	  ACTIVATE_LOADER: null,
	  SET_TOKEN: null,
	  CHANGE_TOPNAV_TITLE: null,
	  LIST_MESSAGES: null,
	})

};
