var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var AppConstants = require('../constants/AppConstants');
var assign = require('object-assign');
var Utils = require('../utils/Services');
var CHANGE_EVENT = 'change';

const DEFAULT_TEXT = 'IM';
const SHOW_BACK_ICON = false;
const LOADER_STATE = {display: 'none'};

const BACK_URL = '/';

var _showBackIcon = SHOW_BACK_ICON;

var _topNavTitle = DEFAULT_TEXT;

var _backURL = BACK_URL;

var _loaderState = LOADER_STATE;

function _update(data) {
  _topNavTitle = data.title || DEFAULT_TEXT;
  _showBackIcon = data.showBackIcon || SHOW_BACK_ICON;
  _backURL = data.backURL || BACK_URL;
}

function _activateLoader(data) {
  _loaderState = data || LOADER_STATE;
}

function _getTitle() {
  return _topNavTitle;
}

function _getBackIcon() {
  return _showBackIcon;
}

function _getBackURL() {
  return _backURL;
}

function _getLoader() {
  return _loaderState;
}

var BaseStore = assign({}, EventEmitter.prototype, {
  getTitle: function () {
    return  _getTitle();
  },

  getBackIcon: function(){
    return _getBackIcon();
  },

  getBackURL: function(){
    return _getBackURL();
  },

  getLoader: function(){
    return _getLoader();
  },
  
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

// Register callback to handle all updates
AppDispatcher.register(function(action) {
  switch(action.type) {
    case AppConstants.ActionTypes.CHANGE_TOPNAV_TITLE:
      _update(action.data);
      BaseStore.emitChange();
      break;
    case AppConstants.ActionTypes.ACTIVATE_LOADER:
      _activateLoader(action.data);
      BaseStore.emitChange();
      break;

    default:
      // no op
  }
});

module.exports = BaseStore;
