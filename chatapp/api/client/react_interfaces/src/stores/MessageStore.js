var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var AppConstants = require('../constants/AppConstants');
var assign = require('object-assign');
var CHANGE_EVENT = 'change';
var _ = require('lodash');

function _getMessages(conversation_id) {
  var messages = localStorage.getItem('conversation.'+conversation_id) || undefined;
  var conversation = [] 
  if (messages != undefined) {
    conversation = JSON.parse(messages);
  }
  return conversation;
}

function _addMessage(data, conversation_id) {
  var messages = localStorage.getItem('conversation.'+conversation_id) || undefined;
  if (messages != undefined) {
    messages = JSON.parse(messages);
    messages.push(data);
  }
  else {
    messages = [data];
  }
  localStorage.setItem('conversation.'+conversation_id, JSON.stringify(messages));
}

function _listMessages(data, conversation_id) {
  var newMessages = data.results;
  var oldMessagesString = localStorage.getItem('conversation.'+conversation_id) || undefined;
  var oldMessages = [];
  if (oldMessagesString != undefined){
    oldMessages = JSON.parse(oldMessagesString);
    oldMessages = _.union(oldMessages, newMessages);
    oldMessages = _.uniq(oldMessages, function (message){
      return message.id;
    });
  }
  localStorage.setItem('conversation.'+conversation_id, JSON.stringify(oldMessages));
}

var MessageStore = assign({}, EventEmitter.prototype, {
  getMessages: function (conversation_id) {
    return  _getMessages(conversation_id);
  },

  addMessage: function(){
    return _addMessage();
  },
  
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

// Register callback to handle all updates
AppDispatcher.register(function(action) {
  switch(action.type) {
    case AppConstants.ActionTypes.ADD_MESSAGE:
      console.log(action);
      _addMessage(action.data, action.conversation_id);
      MessageStore.emitChange();
      break;
    case AppConstants.ActionTypes.LIST_MESSAGES:
      _listMessages(action.data, action.conversation_id);
      MessageStore.emitChange();
      break;

    default:
      // no op
  }
});

module.exports = MessageStore;
