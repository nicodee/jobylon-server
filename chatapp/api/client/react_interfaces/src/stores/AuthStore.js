var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var AppConstants = require('../constants/AppConstants');
var assign = require('object-assign');
var CHANGE_EVENT = 'change';
var Services = require('../utils/Services');
// var user = localStorage.getItem('authenticated-user') || undefined;
// var token = localStorage.getItem('token') || undefined;

// var users = localStorage.getItem('users') || undefined;

function _getUser() {
  var user =  localStorage.getItem('authenticated-user');
  if (user != undefined) {
     return JSON.parse(user);
  }
  return undefined;
}

function setUser(user) {
  localStorage.setItem('authenticated-user', JSON.stringify(user));
}

function _getUsers() {
  var users = localStorage.getItem('users') || undefined;
  if (users != undefined) {
     return JSON.parse(users);
  }
  return [];
}

function setUsers(users) {
  localStorage.setItem('users', JSON.stringify(users.results));
}

function _getToken() {
  var token = localStorage.getItem('token') || undefined;
  if (token != 'undefined' && token != undefined) {
    return token;
  }
  return undefined;
    
}

function setToken(token) {
  localStorage.setItem('token', token);
}

function _logout() {
  // localStorage.removeItem('token');
  // localStorage.removeItem('authenticated-user');
  // localStorage.removeItem('users');
  localStorage.clear();
}

var AuthStore = assign({}, EventEmitter.prototype, {

  getUser: function () {
    return _getUser();
  },

  getUsers: function () {
    return _getUsers();
  },

  logout: function () {
    _logout();
  },

  getToken: function () {
    return _getToken();
  },
  
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  }
});

// Register callback to handle all updates
AppDispatcher.register(function(action) {
  switch(action.type) {
    case AppConstants.ActionTypes.SET_TOKEN:
      setToken(action.data.token);
      AuthStore.emitChange();
      break;
    case AppConstants.ActionTypes.LIST_USERS:
      setUsers(action.data);
      AuthStore.emitChange();
      break;
    case AppConstants.ActionTypes.LIST_USER:
      setUser(action.data);
      AuthStore.emitChange();
      break;

    default:
      // no op
  }
});

module.exports = AuthStore;
