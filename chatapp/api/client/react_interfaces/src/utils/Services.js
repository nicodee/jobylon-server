var ActionsCreators = require('../actions/ActionsCreators');
var AuthStore = require('../stores/AuthStore');



module.exports = {

  getCookie: function(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
          var cookies = document.cookie.split(';');
          for (var i = 0; i < cookies.length; i++) {
              var cookie = $.trim(cookies[i]);
              // Does this cookie string begin with the name we want?
              if (cookie.substring(0, name.length + 1) == (name + '=')) {
                  cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                  break;
              }
          }
      }
      return cookieValue;
  },

  createUser: function(data, callBack) {
    var url = '/api/v1/users/';
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'POST',
      cache: false,
      data: JSON.stringify(data),
      headers: {
        'X-CSRFToken': this.getCookie('csrftoken'),
        'Content-Type': 'application/json'
      },
      success: function (data) {
        console.log(data);
        ActionsCreators.setToken({token: data.auth_token});
        callBack({type: 'success', data: data});
      },
      error: function (xhr, status, err) {
        console.error(url, status, err.toString());
        callBack(
          {
            type: 'error', 
            error: {
              xhr: xhr, 
              status: status, 
              err: err
            }
          });
      }
    });
  },

  getCurrentUser: function(data) {
    var url = '/api/v1/user/';
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + data,
        'Content-Type': 'application/json'
      },
      cache: false,
      success: function (data) {
        ActionsCreators.listUser(data.results[0]);
      },
      error: function (xhr, status, err) {
        console.error(url, status, err.toString());
        // callBack({type: 'error'})
      }
    });
  },

  users: function(data, callBack) {
    var url = '/api/v1/users/';
    var token = data;
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + token,
        'Content-Type': 'application/json'
      },
      cache: false,
      success: function (data) {
        ActionsCreators.listUsers(data);
      },
      error: function (xhr, status, err) {
        console.error(url, status, err.toString());
        // callBack({type: 'error'})
      }
    });
  },

  login: function(data, callBack) {
    var url = '/api/v1/api-token-auth/';
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'POST',
      headers: {
        'X-CSRFToken': this.getCookie('csrftoken'),
        'Content-Type': 'application/json'
      },
      cache: false,
      data: JSON.stringify(data),
      success: function (data) {
        // console.log(data);
        ActionsCreators.setToken(data);
        callBack({type: 'success', data: data});
      },
      error: function (xhr, status, err) {
        console.error(url, status, err.toString());
        callBack({type: 'error'})
      }
    });
  },

  getConversation: function(data, callBack) {
    var url = '/conversation/'+ data.chat_buddy_id;
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + data.token,
        'Content-Type': 'application/json'
      },
      cache: false,
      success: function (data) {
        // ActionsCreators.setToken(data);
        callBack({type: 'success', data: data});
      },
      error: function (xhr, status, err) {
        console.error(url, status, err.toString());
        callBack({type: 'error'})
      }
    });
  },

  getMessages: function(data, callBack) {
    var url = '/api/v1/messages?conversation_id='+ data.conversation_id;
    var conversation_id = data.conversation_id;
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'GET',
      headers: {
        'Authorization': 'Token ' + data.token,
        'Content-Type': 'application/json'
      },
      cache: false,
      success: function (data) {
        ActionsCreators.listMessages(data, conversation_id);
        callBack({type: 'success', data: data});
      },
      error: function (xhr, status, err) {
        console.error(url, status, err.toString());
        callBack({type: 'error'})
      }
    });
  },

  addMessage: function(data) {
    var url = '/api/v1/messages/';
    // var conversation_id = data.conversation_id;
    $.ajax({
      url: url,
      dataType: 'json',
      method: 'POST',
      headers: {
        'Authorization': 'Token ' + AuthStore.getToken(),
        'Content-Type': 'application/json'
      },
      cache: false,
      data: JSON.stringify(data),
      success: function (data) {
        // console.log(data);
        ActionsCreators.addMessage(data, data.conversation)
        // ActionsCreators.listMessages(data, conversation_id);
        // callBack({type: 'success', data: data});
      },
      error: function (xhr, status, err) {
        console.error(url, status, err.toString());
        // callBack({type: 'error'})
      }
    });
  }

};