var React = require("react");
var ReactDOM = require("react-dom");

/**
 * Importing dependencie for touch in material-ui
 */

var injectTapEventPlugin = require("react-tap-event-plugin");
 /**
  * Needed for onTouchTap
  * Can go away when react 1.0 release
  * Check this repo: 
  * https://github.com/zilverline/react-tap-event-plugin 
  */

injectTapEventPlugin();

/**
 * loading react-router
 */
var Router = require('react-router').Router;
var hashHistory = require('react-router').hashHistory;
var routes = require('./routes');

ReactDOM.render(
  <Router history={hashHistory} routes={routes}></Router>, 
  document.getElementById('container')
);

module.hot.accept();