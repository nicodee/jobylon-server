var React = require('react')
var Route = require('react-router').Route;
var IndexRoute = require('react-router').IndexRoute;

var App = require('./components/app');
import Home from './components/Home/Home';
import Login from './components/Auth/Login.jsx';
import Signup from './components/Auth/Signup.jsx';
import Users from './components/Users/UsersList.jsx';
import Conversation from './components/Conversation/Conversation.jsx';

const routes = (
  <Route path="/" component={App}>
    <IndexRoute component={Login} />
    <Route path="login" component={Login} />
    <Route path="users" component={Users} />
    <Route path="signup" component={Signup} />
    <Route path="conversation/:conversation_id/:username/:user_id" component={Conversation} />
  </Route>
);

module.exports = routes;
