var React = require('react');
import TopNav from './TopNav/TopNav';
import SideNav from './SideNav/SideNav';
import Loader from './Loader';
var Link = require('react-router').Link;
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {cyan500} from 'material-ui/styles/colors';

const muiTheme = getMuiTheme({
  // palette: {
  //   // textColor: cyan500,
  // },
  // appBar: {
  //   height: 50,
  // },
});

const App = React.createClass({
	render: function () {
		return (

			<MuiThemeProvider muiTheme={muiTheme}>
				
				<div>
					<TopNav 
					 	onLeftIconButtonTouchTap={this._onMenuIconButtonTouch}
					/>
					<SideNav ref="sideNav" />
					<Loader ref="loader"/>

					{this.props.children}

				</div>

			</MuiThemeProvider>
		)
	},
	_onMenuIconButtonTouch: function() {
		this.refs.sideNav.handleToggle();
	}
});

module.exports = App;