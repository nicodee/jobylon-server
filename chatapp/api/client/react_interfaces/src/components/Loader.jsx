import React from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';
import BaseStore from '../stores/BaseStore';
import autoBind from 'react-autobind';

var loaderStyle = {
	display: 'none',
}

var refreshStyle = {
	margin: '50% 50%'
}

function getStateFromStores(){
	return {
		showLoader: BaseStore.getLoader()
	}
}

class Loader extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Loader';
        autoBind(this);
        this.state = getStateFromStores();
    }

    componentDidMount() {
    	BaseStore.addChangeListener(this.onChange);
    }

    componentWillUnmount() {
    	BaseStore.removeChangeListener(this.onChange);
    }

    onChange() {
    	this.setState(getStateFromStores());
    }

    render() {
        return (
        	<div className="loader" style={this.state.showLoader}>
        		<RefreshIndicator        		
	        		size={50}
	        		left={0}
	        		top={0}
	        		loadingColor={"#FF9800"}
	        		status="loading"
	        		style={refreshStyle}
        		/> 
        	</div>
        );
    }
}

export default Loader;
