import React from 'react';
import Person from 'material-ui/svg-icons/social/person';

import {ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import autoBind from 'react-autobind';
import ActionsCreators from '../../actions/ActionsCreators.js';
import Services from '../../utils/Services';

export default class UserItem extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'UserItem';
        autoBind(this);
    }

    render() {
    	var user = this.props.user;
        var fullname = user.first_name + ' ' + user.last_name;
        return (
        	<div>
	        	<ListItem 
	        		primaryText={user.username}
                    onTouchTap={this.getConversation}
                    leftIcon={<Person />}
                />
                <Divider/>
            </div>
        );
    }


    getConversation () {
        ActionsCreators.activateLoader({display: 'block'});
        Services.getConversation({chat_buddy_id: this.props.user.id, token: this.props.token}, this.getConversationCallback);
    }

    getConversationCallback (response) {
        if(response.type == 'error'){
            console.log(response)
            ActionsCreators.activateLoader({display: 'none'});
        }
        else {
            console.log(response.data);
            if (response.data.results.length = 1){
                this.context.router.push('/conversation/' + response.data.results[0].id + '/' + this.props.user.username + '/' +this.props.user.id);
            }
            else {
                ActionsCreators.activateLoader({display: 'none'});
            }
        }  
    }

    gotoConversation (conversation_id) {
        var url = '/conversation/' + conversation_id;
        this.context.router.push(url);
    }

}


UserItem.contextTypes = {
  router: React.PropTypes.object
};