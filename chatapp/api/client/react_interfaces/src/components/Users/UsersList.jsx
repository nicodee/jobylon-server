import React from 'react';
import UserItem from './UserItem.jsx';
import {List} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import ActionsCreators from '../../actions/ActionsCreators.js';
import BaseStore from '../../stores/BaseStore.js';
import AuthStore from '../../stores/AuthStore.js';
import Divider from 'material-ui/Divider';
import autoBind from 'react-autobind';
import Services from '../../utils/Services';

function getStateFromStores() {
  return {
    users: AuthStore.getUsers(),
    token: AuthStore.getToken()
  };
}

export default class UsersList extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'UsersList';
        this.state = getStateFromStores();
        autoBind(this);
    }

    componentDidMount() {
        BaseStore.addChangeListener(this.onChange);
        AuthStore.addChangeListener(this.onChange);
        ActionsCreators.changeTopNavTitle({title: 'Select A User To Start Chatting With Them', showBackIcon: false});
        var that = this;
        setTimeout(function () {
            that.checkForToken();
        }, 500);
    }

    checkForToken() {
        if (AuthStore.getToken() != undefined) {
            Services.users(AuthStore.getToken());
            ActionsCreators.activateLoader({display: 'block'});
        }
        else{
            AuthStore.logout();
            this.context.router.push('/');
        }
    }

    componentWillUnmount() {
        BaseStore.removeChangeListener(this.onChange);
    	AuthStore.addChangeListener(this.onChange);
    }

    render() {
        var users = this.state.users;
        var userItems = [];
        var length = users.length + 1;

        if (users.length > 0){
	        for (var key in users) {  
	          userItems.push(
	            <UserItem key={key} user={users[key]} token={this.state.token}/>
	          );
	        }
        }

				else{
				  userItems.push(
				      <Subheader key={length}>No items to display</Subheader>
				  );
				}
                
        return (
          <div>
            <List>
              {userItems}
            </List>
          </div>
        )
    }

    onChange(){
    	this.setState(getStateFromStores());
        ActionsCreators.activateLoader({display: 'none'});
    }
}

UsersList.contextTypes = {
  router: React.PropTypes.object
};