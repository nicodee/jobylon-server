import React from 'react';
import autoBind from 'react-autobind';
import ActionsCreators from '../../actions/ActionsCreators';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
var Link = require('react-router').Link;
import AuthStore from '../../stores/AuthStore';
import Services from '../../utils/Services';

const PaperStyle = {
	width: '50%',
	margin: '25%',
	display: 'inline-block',
	padding: '10%',
	marginTop: '10%'
};

var buttonStyle = {
	margin: 12,
	marginTop: 50,
	width: '100%'
};

function getStateFromStores() {
	return {
		token: AuthStore.getToken(),
		username: '',
		password: '',
		confirmPassword: '',
		errorText: '',
		error: true
	}
}

class Signup extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Signup';
        this.state = getStateFromStores();
        autoBind(this);
    }

    componentDidMount() {
    	AuthStore.addChangeListener(this.onChange);    	
        ActionsCreators.changeTopNavTitle({title: 'Signup', showBackIcon: true});
    }

    componentWillUnmount() {    	
    	AuthStore.removeChangeListener(this.onChange);
    }

    onChange() {
    	this.setState(getStateFromStores());
    }

    getPassword(event, value) {
    	this.setState({
    		password: value
    	});
    	var that = this;
    	setTimeout(function(){
	    	that.confirmPassword();
	    	that.canSubmit();
    	}, 100);
    }

    getPassword2(event, value){
    	this.setState({
    		confirmPassword: value
    	});
    	var that = this;
    	setTimeout(function(){
	    	that.confirmPassword();
	    	that.canSubmit();
    	}, 100);
    }

    confirmPassword() {
    	if (this.state.password != this.state.confirmPassword) {
    		this.setState({errorText: 'Passwords don\'t match'});
    	}
    	else {
    		this.setState({errorText: ''});
    	}
    }

    getUsername(event, value) {
    	this.setState({
    		username: value
    	});
    	var that = this;
    	setTimeout(function(){
			that.canSubmit();
		}, 100);
    }

    canSubmit() {
    	if(
    		(this.state.password == this.state.confirmPassword) && 
    		(this.state.username != '') && 
    		(this.state.password != '') && 
    		(this.state.confirmPassword != '')
    	){
    		this.setState({error: false});
    	}
    	else {
    		this.setState({error: true});
    	}
    }

    onSubmit(){
    	ActionsCreators.activateLoader({display: 'block'});
    	Services.createUser(
    		{
    			username: this.state.username,
    			password: this.state.password
    		}, this.createUserCallback);
    }

    createUserCallback(response){
    	console.log(response);
    	if (response.type == 'error'){
    		console.log(response.error);
    		ActionsCreators.activateLoader({display: 'none'});
    	}
    	else {
    		Services.getCurrentUser(response.data.auth_token);
    		this.context.router.push('/users');
    	}
    }

    render() {
        return (
        	<div>
	        	<Paper style={PaperStyle} zDepth={1}>

	        		<div className="row username">
						<div className="col-xs-12">
							<div className="box">
								<TextField									
									hintText="username"
									floatingLabelText="Username"
									type="text"
									className="username"
									fullWidth={true}
									onChange={this.getUsername}
								/>
							</div>
						</div>	        			
	        		</div>

	        		<div className="row password">
						<div className="col-xs-12">
							<div className="box">
								<TextField									
									hintText="password"
									floatingLabelText="Password"
									type="password"
									className="password"
									fullWidth={true}
									onChange={this.getPassword}
								/>
							</div>
						</div>	        			
	        		</div>

	        		<div className="row confirm-password">
						<div className="col-xs-12">
							<div className="box">
								<TextField									
									hintText="confirm password"
									floatingLabelText="Confirm Password"
									type="password"
									errorText={this.state.errorText}
									className="password"
									fullWidth={true}
									onChange={this.getPassword2}
								/>
							</div>
						</div>	        			
	        		</div>

	        		<div className="row signup-button">
	        			<div className="col-xs-offset-3 col-xs-6">
	        				<div className="box">
	        					<RaisedButton 
	        						label="Signup" 
	        						style={buttonStyle}
	        						disabled={this.state.error}
	        						onTouchTap={this.onSubmit}
	        					/>
	        				</div>
	        			</div>
	        		</div>

	        		<br></br>

	        		<div className="row">
	        			<div className="col-xs-12">
	        				<div className="box">
				              <Link to='/login'>
				                  Already have an account? Login Here.
				              </Link>
	        				</div>
	        			</div>
	        		</div>

	        	</Paper>
        	</div>
        );
    }
}

export default Signup;


Signup.contextTypes = {
  router: React.PropTypes.object
};