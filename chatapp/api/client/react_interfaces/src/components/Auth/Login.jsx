import React from 'react';
import autoBind from 'react-autobind';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import ActionsCreators from '../../actions/ActionsCreators';
import Services from '../../utils/Services';
import AuthStore from '../../stores/AuthStore';
var Link = require('react-router').Link; 

const PaperStyle = {
	width: '60%',
	margin: '20%',
	display: 'inline-block',
	padding: '10%',
	marginTop: '10%'
};

var buttonStyle = {
	margin: 12,
	marginTop: 50,
	width: '100%'
};

function getStateFromStores() {
	return {
		token: AuthStore.getToken(),
		username: '',
		password: ''
	}
}

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Login';
        autoBind(this);
        this.state = {
        	username: '',
        	password: '',
        	token: undefined
        }
    }

    componentDidMount() {
    	AuthStore.addChangeListener(this.onChange);
        ActionsCreators.changeTopNavTitle({title: 'Login', showBackIcon: false});
    	this.checkForToken();
    }

    componentWillUnmount() {
    	AuthStore.removeChangeListener(this.onChange);
    }

    checkForToken() {
    	if (AuthStore.getToken() != undefined) {
    		ActionsCreators.activateLoader({display: 'block'});
    		Services.getCurrentUser(AuthStore.getToken());
	    	this.context.router.push('/users');
    	}
    }

    onChange() {
    	this.setState(getStateFromStores());
    }

    onSubmit() {
    	if (this.state.password != '' && this.state.username != '') {
	    	ActionsCreators.activateLoader({display: 'block'});
	    	Services.login({username: this.state.username, password: this.state.password}, this.loginCallback);
	    	
    	}
    }

    loginCallback(response) {
    	if(response.type == 'error') {
    		ActionsCreators.activateLoader({display: 'none'});
    	}
    	else {
			Services.getCurrentUser(response.data.token);
    		this.context.router.push('/users');
    	}
    }

    getPassword(event, value) {
    	this.setState({
    		password: value
    	});
    }

    getUsername(event, value) {
    	this.setState({
    		username: value
    	});
    }

    render() {
        return (
        	<div>
	        	<Paper style={PaperStyle} zDepth={1}>

	        		<div className="row username">
						<div className="col-xs-12">
							<div className="box">
								<TextField									
									hintText="username"
									floatingLabelText="Username"
									type="text"
									className="username"
									fullWidth={true}
									onChange={this.getUsername}
								/>
							</div>
						</div>	        			
	        		</div>

	        		<div className="row password">
						<div className="col-xs-12">
							<div className="box">
								<TextField									
									hintText="password"
									floatingLabelText="Password"
									type="password"
									className="password"
									fullWidth={true}
									onChange={this.getPassword}
								/>
							</div>
						</div>	        			
	        		</div>

	        		<div className="row login-button">
	        			<div className="col-xs-offset-3 col-xs-6">
	        				<div className="box">
	        					<RaisedButton 
	        						label="Login" 
	        						style={buttonStyle} 
	        						onTouchTap={this.onSubmit}
	        					/>
	        				</div>
	        			</div>
	        		</div>

	        		<br></br>

	        		<div className="row">
	        			<div className="col-xs-12">
	        				<div className="box">
				              <Link to='/signup'>
				                  Don't have an account? Signup Here.
				              </Link>
	        				</div>
	        			</div>
	        		</div>

	        	</Paper>
        	</div>
        );
    }
}

export default Login;


Login.contextTypes = {
  router: React.PropTypes.object
};
