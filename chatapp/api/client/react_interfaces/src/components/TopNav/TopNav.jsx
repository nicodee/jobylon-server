import React from 'react';var AppBar = require('material-ui').AppBar;
var BaseStore = require('../../stores/BaseStore.js');
import Back from 'material-ui/svg-icons/navigation/arrow-back';
import Menu from 'material-ui/svg-icons/navigation/menu';
import IconButton from 'material-ui/IconButton';

const topNavStyle = {
	backgroundColor: 'rgb(243, 124, 34)'
};

function getStateFromStores(){
	return {
		title: BaseStore.getTitle(),
		showBackIcon: BaseStore.getBackIcon(),
		backURL: BaseStore.getBackURL()
	};
}

export default class TopNav extends React.Component {
    constructor(props) {
        super(props);
        this.state = getStateFromStores();
        this.displayName = 'TopNav';
        this._onChange = this._onChange.bind(this);
        this._goBack = this._goBack.bind(this);
    }	

    componentDidMount () {
		BaseStore.addChangeListener(this._onChange);
	}

	componentWillUnmount () {
		BaseStore.removeChangeListener(this._onChange);	
	}

	render () {
		var icon, leftIconClicked, iconClassName;
		if (this.state.showBackIcon == true){
			leftIconClicked = this._goBack;
			icon = (<IconButton onTouchTap={leftIconClicked}><Back/></IconButton>);
		}
		else {
			leftIconClicked = this.props.onLeftIconButtonTouchTap;
			icon = (<IconButton onTouchTap={leftIconClicked}><Menu/></IconButton>);
		}
		return (
			<AppBar
			  title={this.state.title}
			  iconElementLeft={icon}
			  onLeftIconButtonTouchTap={leftIconClicked}
			  style={topNavStyle}
			/>
		)
	}

	_onChange () {
		this.setState(getStateFromStores());
	}

	_goBack (e) {
		this.context.router.push(this.state.backURL);
	}
}


TopNav.contextTypes = {
  router: React.PropTypes.object
};
