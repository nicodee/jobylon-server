import React from 'react';
// import {ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import autoBind from 'react-autobind';
import ActionsCreators from '../../actions/ActionsCreators.js';
import Services from '../../utils/Services';
import AuthStore from '../../stores/AuthStore.js';
import MessageStore from '../../stores/MessageStore.js';
import Message from './Message.jsx';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import Send from 'material-ui/svg-icons/content/send';

function getStateFromStores(conversation_id){
	return {
		messages: MessageStore.getMessages(conversation_id),
		content: '',
		user: AuthStore.getUser()
	}
}

class Conversation extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Conversation';
        this.state = getStateFromStores(this.props.params.conversation_id);
        autoBind(this);
    }

    componentDidMount() {
    	MessageStore.addChangeListener(this.onChange);     
    	AuthStore.addChangeListener(this.onChange);     
        ActionsCreators.changeTopNavTitle({title: this.props.params.username, showBackIcon: true});
        ActionsCreators.activateLoader({display: 'block'});
        Services.getMessages({
        	conversation_id: this.props.params.conversation_id,
        	token: AuthStore.getToken()
        }, this.getMessagesCallback);  
    }

    componentWillUnmount() {
    	MessageStore.removeChangeListener(this.onChange);
    	AuthStore.removeChangeListener(this.onChange);
    }

    onChange() {
    	this.setState(getStateFromStores(this.props.params.conversation_id));
    }

    getMessagesCallback(response) {
		if (response.type=='error'){
			console.log(response);
    	}
    	else {
    		ActionsCreators.listMessages(response.data, this.props.params.conversation_id);
    	}
		ActionsCreators.activateLoader({display: 'none'});
    }

    getMessage(event, value) {
    	this.setState({content: value});
    }

	sendMessage(){
		if(this.state.content == '') {
			alert('Type in a message first');
		}
		else {
			var date =  new Date();

			var data = {
				conversation: this.props.params.conversation_id,
				content: this.state.content,
				created: date.toISOString(),
				receipient: this.props.params.user_id
			}
			Services.addMessage(data);
		}
	}

    render() {
    	const messages  = [];

    	for (var key in this.state.messages) {  
    	  messages.push(
    	    <Message key={key} message={this.state.messages[key]} user={this.state.user}/>
    	  );
    	}
        return (
        	<div className="chatBox">
        		<div className="messages-container">
        			{messages}
        		</div>

        		<div className="text-area row bottom-xs">
	        		<div className="col-xs-11">
		        		<div className="box">
		        			<TextField
			        			hintText='type your message in here .....'
		        				fullWidth={true}
		        				multiLine={true}
		        				value={this.state.content}
		        				onChange={this.getMessage}
		        			/>
		        		</div>
	        		</div>
	        		<div className="col-xs-1">
		        		<div className="box">
		        			<IconButton
		        				onTouchTap={this.sendMessage}
		        			>
	        			      <Send />
	        			    </IconButton>
		        		</div>
	        		</div>
        		</div>
        		
        	</div>
        );
    }
}

export default Conversation;
