import React from 'react';
import Divider from 'material-ui/Divider';
var moment = require('moment');

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Message';
        this.state = {
        	messageType: 'message',
        	created: ''
        }
    }

    componentDidMount() {
    	if (this.props.message.receipient){
    		var messageType = (this.props.user.id == this.props.message.receipient.id)? 'message received' : 'message sent';
    	}
    	else {
    		var messageType = 'message sent';
		}	
		this.setState({messageType: messageType});
		var created = moment(this.props.message.created, 'YYYY-MM-DDTHH:mm:ssZ').fromNow();
		this.setState({created: created});
	}

    render() {
    	return (
        	<div className="message-container">
	        	<div className={this.state.messageType}>
	        		<p>
			    		{this.props.message.content}
	        		</p>
	        		<small className="time"><span>{this.state.created}</span></small>
	        	</div>
	        	<br></br>
        	</div>
        );
    }
}

export default Message;
