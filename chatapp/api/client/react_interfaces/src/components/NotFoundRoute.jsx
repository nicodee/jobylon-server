import React from 'react';
import Snackbar from 'material-ui/Snackbar';

class NotFoundRoute extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'NotFoundRoute';
        this.handleRequestClose = this.handleRequestClose.bind(this);
        this._goToHome = this._goToHome.bind(this);
        this.state = {
		      open: false,
		    };
    }

    componentWillMount() {
    	this._goToHome();
    }

  	handleRequestClose() {
  		this.setState({
  		  open: false,
  		});
  	}

    render() {
        return (
        	<div>
        		<Snackbar
		          open={this.state.open}
		          message="That page does not exist. You'll be redirected in a few seconds."
		          autoHideDuration={10000}
		          onRequestClose={this.handleRequestClose}
		        />
        	</div>
        );
    }

    _goToHome(){
      // this.context.router.goBack();
      window.location = "/404/";
    }
}

export default NotFoundRoute;

// NotFoundRoute.contextTypes = {
//   router: React.PropTypes.object
// };