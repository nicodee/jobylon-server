var React = require('react');
var Drawer = require('material-ui').Drawer ;
var MenuItem = require('material-ui').MenuItem;
var RaisedButton = require('material-ui').RaisedButton;
var Link = require('react-router').Link; 
import Exit from 'material-ui/svg-icons/action/exit-to-app';
import Add from 'material-ui/svg-icons/content/add-circle-outline';
import Home from 'material-ui/svg-icons/action/home';
import People from 'material-ui/svg-icons/social/people';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import Business from 'material-ui/svg-icons/communication/business';
import Input from 'material-ui/svg-icons/action/input';
import autoBind from 'react-autobind';
import AuthStore from '../../stores/AuthStore';

class SideNav extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = '';
        this.state = {
          open: false,
          showLinks: {display: 'block', textDecoration: 'none'}
        };
        autoBind(this);
        this.linkStyle = {
          textDecoration: 'none'
        };
    }

    handleToggle() {
      this.setState({open: !this.state.open});
    }

    handleClose() {
      this.setState({open: false});
    }

    handleLogOut() {
      window.location = "/mutti/crm/webform/logout/";
    }
     
    componentDidMount() {
      AuthStore.addChangeListener(this.onChange);
      this.checkForToken();
    }

    componentWillUnMount() {
      AuthStore.removeChangeListener(this.onChange);      
    }

    onChange() {
      // console.log('hello');
      this.checkForToken();
    }

    checkForToken() {
      if (AuthStore.getToken() != undefined){
        this.setState({showLinks: {display: 'block', textDecoration: 'none'}})
      }
      else{
        this.setState({showLinks: {display: 'none', textDecoration: 'none'}})
      }
    }

    logout () {
      AuthStore.logout();
      // this.context.router.push('/');
      this.handleClose();
      this.checkForToken();
    }

    render() {
      return (
        <div>
          <Drawer 
            open={this.state.open}
            docked={false}            
            onRequestChange={open => this.setState({open})}
            >
            <Link to='/' style={this.linkStyle}>
              <MenuItem onTouchTap={this.handleClose} leftIcon={<Home/>}>
                Home
              </MenuItem>
            </Link>
            <Link to='/' style={this.linkStyle}>
              <MenuItem onTouchTap={this.logout} leftIcon={<Input/>} style={this.state.showLinks}>
                Logout
              </MenuItem>
            </Link>
            {/*
              <Link to='/login' style={this.state.showLinks}>
                <MenuItem onTouchTap={this.handleClose} leftIcon={<People/>}>
                  Login
                </MenuItem>
              </Link>
              <Link to='/signup' style={this.state.showLinks}>
                <MenuItem onTouchTap={this.handleClose} leftIcon={<PersonAdd/>}>
                  Signup
                </MenuItem>
              </Link>
            */}
            {/*<MenuItem onTouchTap={this.handleLogOut} leftIcon={<Exit />}>
                          <a href="/logout" className="logOut">Log out</a>
                        </MenuItem>*/}
          </Drawer>
        </div>
      );    
    }
}

export default SideNav;

SideNav.contextTypes = {
  router: React.PropTypes.object
};
