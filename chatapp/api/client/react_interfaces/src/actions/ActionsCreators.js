var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var ActionTypes = AppConstants.ActionTypes;

module.exports = {
	
	changeTopNavTitle: function(data) {
		setTimeout(function() {
			AppDispatcher.dispatch({
				type: ActionTypes.CHANGE_TOPNAV_TITLE,
				data: data
			});
		}, 100);
	},

	activateLoader: function(data) {
		setTimeout(function() {
			AppDispatcher.dispatch({
				type: ActionTypes.ACTIVATE_LOADER,
				data: data
			});
		}, 100);
	},

	setToken: function(data) {
		setTimeout(function() {
			AppDispatcher.dispatch({
				type: ActionTypes.SET_TOKEN,
				data: data
			});
		}, 100);
	},

	listUsers: function(data) {
		setTimeout(function(){
			AppDispatcher.dispatch({
				type: ActionTypes.LIST_USERS,
				data: data
			});
		},100);
	},

	listUser: function(data) {
		setTimeout(function(){
			AppDispatcher.dispatch({
				type: ActionTypes.LIST_USER,
				data: data
			});
		},100);
	},

	listMessages: function(data, conversation_id) {
		setTimeout(function(){
			AppDispatcher.dispatch({
				type: ActionTypes.LIST_MESSAGES,
				data: data,
				conversation_id: conversation_id
			});
		},100);		
	},

	addMessage: function(data, conversation_id) {
		setTimeout(function(){
			AppDispatcher.dispatch({
				type: ActionTypes.ADD_MESSAGE,
				data: data,
				conversation_id: conversation_id
			});
		},100);		
	}

}