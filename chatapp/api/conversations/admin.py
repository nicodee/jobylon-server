from django.contrib import admin
from conversations.models import Conversation, Message

# class Conversation(admin.ModelAdmin):
  # inlines=[
    # StockInline,
  # ]
  # search_fields = ('pharmacy',)
  # list_filter = ('drug__country', 'pharmacy')

admin.site.register(Conversation)
admin.site.register(Message)