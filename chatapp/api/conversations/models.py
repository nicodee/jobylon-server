from __future__ import unicode_literals
from users.models import User
from django.db import models

# Create your models here.

class Conversation(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    members = models.ManyToManyField(User, blank=True,
                                      related_name=
                                      "%(app_label)s_%(class)s_related")

    # def save(self, *args, **kwargs):
    # 	print dir(self)
    # 	print dir(*args)

    def __str__(self):
    	return str(self.members.all())


class Message(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    conversation = models.ForeignKey(Conversation)
    content = models.TextField(blank=False)
    receipient = models.ForeignKey(User)
