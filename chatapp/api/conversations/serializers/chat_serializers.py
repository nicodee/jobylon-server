from conversations.models import Conversation, Message
from rest_framework import serializers
from users.serializers import UserSerializer


class ConversationSerializer(serializers.ModelSerializer):
	class Meta:
		model = Conversation
		fields = ('id','members', 'created',)


class ConversationReadSerializer(serializers.ModelSerializer):
	members = UserSerializer(many=True)
	class Meta:
		model = Conversation
		fields = ('id','members', 'created',)

class MessageSerializer(serializers.ModelSerializer):
	class Meta:
		model = Message
		fields = ('id','created', 'conversation', 'content', 'receipient')

class MessageReadSerializer(serializers.ModelSerializer):
	receipient = UserSerializer(many=False)
	conversation = ConversationReadSerializer(many=False)
	class Meta:
		model = Message
		fields = ('id','created', 'conversation', 'content', 'receipient')