from users.models import User
from .models import Message, Conversation
from rest_framework import viewsets, status, generics
from serializers.chat_serializers import MessageSerializer, ConversationSerializer, ConversationReadSerializer, MessageReadSerializer
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

class ConversationViewSet(viewsets.ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = Conversation.objects.all()
	serializer_class = ConversationSerializer

class MessageViewSet(viewsets.ModelViewSet):
	permission_classes = (IsAuthenticated,)
	queryset = Message.objects.all()

	def create(self, request, *args, **kwargs):
		try:
			receipient = User.objects.filter(id=request.data['receipient']).get()
			sender = User.objects.filter(id=request.user.id).get()
			if ((Conversation.objects
				.filter(pk=request.data['conversation'])
				.filter(members=sender.pk)
				.filter(members=receipient.pk).count() == 1) and 
				receipient != sender):
				return super(MessageViewSet, self).create(request, *args, **kwargs)
			else:
				response = {"error": "Conversation doesn't exist"}
				status_code = status.HTTP_404_NOT_FOUND
			
		except Exception, e:
			response = {"error": str(e)}
			status_code = status.HTTP_404_NOT_FOUND

		return Response(response, status=status_code)

	def get_queryset(self):
		"""
		Restricts the returned messages to a given conversation,
		by filtering against a `conversation_id` query parameter in the URL.
		"""
		queryset = Message.objects.all()
		conversation_id = self.request.query_params.get('conversation_id', None)
		if conversation_id is not None:
			queryset = queryset.filter(conversation__id=conversation_id)
			serializer_class = MessageReadSerializer
		return queryset

	def get_serializer_class(self):
		if self.action == 'list':
			return MessageReadSerializer
		return MessageSerializer


class ConversationList(generics.ListAPIView):
	serializer_class = ConversationReadSerializer
	def get_queryset(self):
		chat_buddy_id = self.kwargs['chat_buddy_id']
		chat_buddy_queryset = User.objects.filter(id=chat_buddy_id)
		if chat_buddy_queryset.count() == 0:
			response = {"error": "User doesn't exist"}
			status_code = status.HTTP_404_NOT_FOUND
			return Response(response, status=status)
		else:
			chat_buddy = chat_buddy_queryset.get()
		convo = Conversation.objects.filter(members=chat_buddy.id).filter(members=self.request.user.id)
		if convo.count() == 1:
			return convo
		else:
			if chat_buddy.id == self.request.user.id:
				return []
			new_convo = Conversation()
			new_convo.save()
			new_convo.members.add(chat_buddy, self.request.user)
			# members=[chat_buddy_id,self.request.user.id]
			# # new_convo.save()
			return Conversation.objects.filter(id=new_convo.id)