from rest_framework import viewsets, mixins, generics
from rest_framework.permissions import AllowAny

from .models import User
from .permissions import IsOwnerOrReadOnly
from .serializers import CreateUserSerializer, UserSerializer


class UserViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """
    Creates, Updates, and retrives User accounts
    """
    queryset = User.objects.all().exclude()
    serializer_class = UserSerializer
    permission_classes = (IsOwnerOrReadOnly,)

    def create(self, request, *args, **kwargs):
        self.serializer_class = CreateUserSerializer
        self.permission_classes = (AllowAny,)
        return super(UserViewSet, self).create(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        self.queryset = User.objects.all().exclude(id=request.user.id)
        return super(UserViewSet, self).list(request, *args, **kwargs)

class CurrentUserViewSet(generics.ListAPIView):
    """
    Retrieves currently signed in user's accounts
    """
    serializer_class = UserSerializer
    permission_classes = (IsOwnerOrReadOnly,)

    def get_queryset(self):
      user = self.request.user
      return User.objects.filter(id=user.id)
